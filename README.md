# Self Cart Manager
------------------------
- Simple app with mock API. App contains list of items for example goods in your cart.

#### Tech Stack
1. Kotlin.
2. Retrofit2.
3. Moshi.
4. Coroutines.
5. RoomDB.
6. MVVM.

#### Test Cases
- Need to test repository for check correct work of datastores.
- Need to test a ViewModel for check business logic for correct communication with UI.
- Need to test a consistency of data when receive from api, after wrote to DB, 
  read from DB and check of size and content.

`P.S. folder with test contains several test for repo and for ViewModel.`  
