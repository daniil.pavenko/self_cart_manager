package com.dansdev.selfcartmanager

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.dansdev.selfcartmanager.data.repository.ICartDataSource
import com.dansdev.selfcartmanager.data.repository.ICartRepository
import com.dansdev.selfcartmanager.data.repository.impl.CartRepositoryImpl
import com.dansdev.selfcartmanager.domain.api.util.NetworkManager
import com.dansdev.selfcartmanager.domain.model.Cart
import com.dansdev.selfcartmanager.ui.cart.CartDetailViewModel
import com.dansdev.selfcartmanager.util.getOrAwaitValue
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.TestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.rules.TestWatcher
import org.junit.runner.Description
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.kotlin.*
import kotlin.properties.Delegates

@ExperimentalCoroutinesApi
@RunWith(JUnit4::class)
class CartDetailViewModelTest : TestWatcher() {

    companion object {
        private const val TOTAL_COUNT = 100
    }

    private val testDispatcher: TestDispatcher = StandardTestDispatcher()
    private var cartDetailViewModel: CartDetailViewModel by Delegates.notNull()
    private var repository: ICartRepository by Delegates.notNull()

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private val testCart = Cart(cartTotal = TOTAL_COUNT)

    private val networkManager: NetworkManager = mock {
        on { hasConnection() } doReturn true
    }
    private val remoteDataSource: ICartDataSource = mock {
        onBlocking { getCart() } doReturn Result.success(testCart)
    }
    private val localDataSource: ICartDataSource = mock()

    override fun starting(description: Description?) {
        super.starting(description)
        Dispatchers.setMain(testDispatcher)
    }

    override fun finished(description: Description?) {
        super.finished(description)
        Dispatchers.resetMain()
    }

    @Before
    fun setup() {
        repository = CartRepositoryImpl(localDataSource, remoteDataSource, networkManager)
        cartDetailViewModel = CartDetailViewModel(repository)
    }

    @Test
    fun testCartContainsData() = runBlocking<Unit> {
        cartDetailViewModel.load()

        val cartTotal = cartDetailViewModel.onCartTotalUpdate().getOrAwaitValue()
        val items = cartDetailViewModel.onItemsUpdate().getOrAwaitValue()

        Assert.assertEquals(TOTAL_COUNT, cartTotal)
        verify(remoteDataSource, times(1)).getCart()
        verify(localDataSource, times(1)).saveCart(any())
        verify(networkManager, times(1)).hasConnection()
        Assert.assertSame(testCart, testCart)
    }
}
