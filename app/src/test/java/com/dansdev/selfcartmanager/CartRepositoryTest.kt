package com.dansdev.selfcartmanager

import com.dansdev.selfcartmanager.data.repository.ICartDataSource
import com.dansdev.selfcartmanager.data.repository.ICartRepository
import com.dansdev.selfcartmanager.data.repository.impl.CartRepositoryImpl
import com.dansdev.selfcartmanager.domain.api.util.NetworkManager
import com.dansdev.selfcartmanager.domain.model.Cart
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.TestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.Test
import org.junit.rules.TestWatcher
import org.junit.runner.Description
import org.mockito.kotlin.*

@OptIn(ExperimentalCoroutinesApi::class)
class CartRepositoryTest : TestWatcher() {

    private val testDispatcher: TestDispatcher = StandardTestDispatcher()

    override fun starting(description: Description?) {
        super.starting(description)
        Dispatchers.setMain(testDispatcher)
    }

    override fun finished(description: Description?) {
        super.finished(description)
        Dispatchers.resetMain()
    }

    @Test
    fun testCartRepoWithNetwork() = runBlocking<Unit> {
        val networkManager: NetworkManager = mock {
            on { hasConnection() } doReturn true
        }
        val remoteDataSource: ICartDataSource = mock()
        val localDataSource: ICartDataSource = mock()

        val repository: ICartRepository = CartRepositoryImpl(
            localDataSource,
            remoteDataSource,
            networkManager
        )

        repository.getCart()

        verify(remoteDataSource, times(1)).getCart()
        verify(localDataSource, never()).getCart()
        verify(networkManager, times(1)).hasConnection()
    }

    @Test
    fun testCartRepoWithNetworkAndData() = runBlocking<Unit> {
        val networkManager: NetworkManager = mock {
            on { hasConnection() } doReturn true
        }
        val remoteDataSource: ICartDataSource = mock {
            onBlocking { getCart() } doReturn Result.success(Cart())
        }
        val localDataSource: ICartDataSource = mock()

        val repository: ICartRepository = CartRepositoryImpl(
            localDataSource,
            remoteDataSource,
            networkManager
        )

        repository.getCart()

        verify(remoteDataSource, times(1)).getCart()
        verify(localDataSource, never()).getCart()
        verify(networkManager, times(1)).hasConnection()
    }

    @Test
    fun testCartRepoWithoutNetwork() = runBlocking<Unit> {
        val networkManager: NetworkManager = mock {
            on { hasConnection() } doReturn false
        }
        val remoteDataSource: ICartDataSource = mock()
        val localDataSource: ICartDataSource = mock()

        val repository: ICartRepository = CartRepositoryImpl(
            localDataSource,
            remoteDataSource,
            networkManager
        )

        repository.getCart()

        verify(remoteDataSource, never()).getCart()
        verify(localDataSource, times(1)).getCart()
        verify(networkManager, times(1)).hasConnection()
    }
}
