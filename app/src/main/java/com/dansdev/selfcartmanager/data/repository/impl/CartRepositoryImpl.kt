package com.dansdev.selfcartmanager.data.repository.impl

import com.dansdev.selfcartmanager.data.repository.ICartDataSource
import com.dansdev.selfcartmanager.data.repository.ICartRepository
import com.dansdev.selfcartmanager.domain.api.util.NetworkManager
import com.dansdev.selfcartmanager.domain.model.Cart

class CartRepositoryImpl(
    private val localDataSource: ICartDataSource,
    private val remoteDataSource: ICartDataSource,
    private val networkManager: NetworkManager
) : ICartRepository {

    override suspend fun getCart(): Result<Cart> {
        return try {
            if (networkManager.hasConnection()) {
                val cartResult = remoteDataSource.getCart()
                if (cartResult.isSuccess) cartResult.getOrNull()?.let { cart ->
                    localDataSource.saveCart(cart)
                }
                cartResult
            } else {
                localDataSource.getCart()
            }
        } catch (e: Exception) {
            Result.failure(e.fillInStackTrace())
        }
    }
}
