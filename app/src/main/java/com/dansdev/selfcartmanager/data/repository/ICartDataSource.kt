package com.dansdev.selfcartmanager.data.repository

import com.dansdev.selfcartmanager.domain.model.Cart

interface ICartDataSource {

    suspend fun getCart(): Result<Cart>

    suspend fun saveCart(cart: Cart)
}
