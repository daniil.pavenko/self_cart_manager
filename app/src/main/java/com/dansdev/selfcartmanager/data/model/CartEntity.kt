package com.dansdev.selfcartmanager.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.dansdev.selfcartmanager.domain.model.Cart
import java.util.*

@Entity(tableName = "carts_table")
data class CartEntity(
    @PrimaryKey val id: Long,
    val cartTotal: Int,
    val total: Int,
    val deliveryFee: Int,
    val createdAt: Date?,
    val lastTimeModifiedTimestamp: Long,
    val deliveryDate: Date?,
    val status: Cart.Status?,
    val restaurantId: Long,
    val note: String?,
    val sameDayChargeAmount: Int,
    val subTotal: Int,
    val errorDescription: String?
)
