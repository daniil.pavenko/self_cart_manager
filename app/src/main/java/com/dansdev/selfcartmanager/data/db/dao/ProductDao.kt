package com.dansdev.selfcartmanager.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import com.dansdev.selfcartmanager.data.model.ProductEntity

@Dao
abstract class ProductDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract fun insert(product: ProductEntity)
}
