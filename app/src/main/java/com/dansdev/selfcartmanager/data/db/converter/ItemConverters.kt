package com.dansdev.selfcartmanager.data.db.converter

import androidx.room.TypeConverter
import com.dansdev.selfcartmanager.domain.model.Item

class ItemConverters {

    @TypeConverter
    fun mapPackagingTypeToString(packagingType: Item.PackagingType): String = packagingType.name

    @TypeConverter
    fun mapStringToPackagingType(packagingType: String): Item.PackagingType =
        Item.PackagingType.valueOf(packagingType)

}
