package com.dansdev.selfcartmanager.data.db.dao

import androidx.room.*
import com.dansdev.selfcartmanager.data.model.CartEntity

@Dao
abstract class CartDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract suspend fun insert(cart: CartEntity)

    @Update(onConflict = OnConflictStrategy.IGNORE)
    abstract suspend fun update(cart: CartEntity)

    @Query("SELECT * FROM carts_table")
    abstract suspend fun all(): List<CartEntity>

}
