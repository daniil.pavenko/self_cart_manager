package com.dansdev.selfcartmanager.data.repository

import com.dansdev.selfcartmanager.domain.model.Cart

interface ICartRepository {

    suspend fun getCart(): Result<Cart>
}
