package com.dansdev.selfcartmanager.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.dansdev.selfcartmanager.domain.model.Item

@Entity(tableName = "items_table")
data class ItemEntity(
    @PrimaryKey val id: Long,
    val cartId: Long,
    val quantity: Int,
    val productId: Long,
    val subTotal: Int,
    val orderId: Int,
    val packagingType: Item.PackagingType,
    val substitutable: Boolean,
)
