package com.dansdev.selfcartmanager.data.repository.impl

import com.dansdev.selfcartmanager.data.repository.ICartDataSource
import com.dansdev.selfcartmanager.domain.api.SelfCartService
import com.dansdev.selfcartmanager.domain.api.asKotlinResult
import com.dansdev.selfcartmanager.domain.model.Cart

class RemoteCartDataSource(private val api: SelfCartService) : ICartDataSource {

    override suspend fun getCart(): Result<Cart> {
        val apiResult = api.loadCart()
        return apiResult.asKotlinResult()
    }

    override suspend fun saveCart(cart: Cart) = Unit
}
