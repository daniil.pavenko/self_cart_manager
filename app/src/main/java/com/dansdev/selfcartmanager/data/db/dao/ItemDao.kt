package com.dansdev.selfcartmanager.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.dansdev.selfcartmanager.data.model.ItemEntity
import com.dansdev.selfcartmanager.data.model.ProductEntity

@Dao
abstract class ItemDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract suspend fun insert(items: List<ItemEntity>)

    @Query("SELECT * FROM items_table WHERE cartId == :cartId")
    abstract suspend fun getAllByCartId(cartId: Long): List<ItemEntity>

    @Query(
        "SELECT * FROM items_table " +
                "JOIN products_table ON items_table.productId = products_table.id"
    )
    abstract fun getItemsWithProduct(): Map<ItemEntity, ProductEntity>
}
