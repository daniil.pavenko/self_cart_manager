package com.dansdev.selfcartmanager.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "products_table")
data class ProductEntity(
    @PrimaryKey val id: Long,
    val itemId: Long,
    val name: String,
    val sku: Int,
    val unitPhotoFilename: String?,
    val weightPhotoFilename: String?,
    val packPhotoFile: String?,
    val unitPrice: Int,
    val casePrice: Int,
    val weightPrice: Int,
)
