package com.dansdev.selfcartmanager.data.repository.exception

class EmptyCartException: Throwable("Cart is empty")
