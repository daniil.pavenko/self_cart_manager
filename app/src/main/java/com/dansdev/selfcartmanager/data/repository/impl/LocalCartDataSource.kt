package com.dansdev.selfcartmanager.data.repository.impl

import com.dansdev.selfcartmanager.data.db.AppDatabase
import com.dansdev.selfcartmanager.data.model.CartEntity
import com.dansdev.selfcartmanager.data.model.ItemEntity
import com.dansdev.selfcartmanager.data.model.ProductEntity
import com.dansdev.selfcartmanager.data.repository.ICartDataSource
import com.dansdev.selfcartmanager.data.repository.exception.EmptyCartException
import com.dansdev.selfcartmanager.domain.model.Cart
import com.dansdev.selfcartmanager.domain.model.Item
import com.dansdev.selfcartmanager.domain.model.Product

class LocalCartDataSource(private val db: AppDatabase) : ICartDataSource {

    override suspend fun getCart(): Result<Cart> {
        val cart = db.cartDao().all().lastOrNull()
            ?: return Result.failure(EmptyCartException())
        val itemsAndProduct = db.itemsDao().getItemsWithProduct()
        val items = mutableListOf<Item>()
        itemsAndProduct.entries.forEach { entry ->
            items.add(entry.mapToModel())
        }
        return Result.success(
            Cart(
                cart.id,
                cart.cartTotal,
                cart.total,
                cart.deliveryFee,
                cart.createdAt,
                cart.lastTimeModifiedTimestamp,
                cart.deliveryDate,
                cart.status,
                items,
                cart.restaurantId,
                cart.note,
                cart.sameDayChargeAmount,
                cart.subTotal,
                cart.errorDescription
            )
        )
    }

    override suspend fun saveCart(cart: Cart) {
        db.cartDao().insert(cart.toEntity())
        db.itemsDao().insert(cart.orderItemsInformation.map {
            db.productDao().insert(it.product.toEntity(it.id))
            it.toEntity(cart.id)
        })
    }
}

private fun Map.Entry<ItemEntity, ProductEntity>.mapToModel(): Item = Item(
    this.key.id,
    this.key.quantity,
    this.key.productId,
    this.key.subTotal,
    this.key.orderId,
    this.key.packagingType,
    this.key.substitutable,
    Product(
        this.value.id,
        this.value.name,
        this.value.sku,
        this.value.unitPhotoFilename,
        this.value.weightPhotoFilename,
        this.value.packPhotoFile,
        this.value.unitPrice,
        this.value.casePrice,
        this.value.weightPrice
    )
)

private fun Cart.toEntity() = CartEntity(
    this.id,
    this.cartTotal,
    this.total,
    this.deliveryFee,
    this.createdAt,
    this.lastTimeModifiedTimestamp,
    this.deliveryDate,
    this.status,
    this.restaurantId,
    this.note,
    this.sameDayChargeAmount,
    this.subTotal,
    this.errorDescription
)

private fun Product.toEntity(itemId: Long) = ProductEntity(
    this.id,
    itemId,
    this.name,
    this.sku,
    this.unitPhotoFilename,
    this.weightPhotoFilename,
    this.packPhotoFile,
    this.unitPrice,
    this.casePrice,
    this.weightPrice
)

private fun Item.toEntity(cartId: Long) = ItemEntity(
    this.id,
    cartId,
    this.quantity,
    this.productId,
    this.subTotal,
    this.orderId,
    this.packagingType,
    this.substitutable
)
