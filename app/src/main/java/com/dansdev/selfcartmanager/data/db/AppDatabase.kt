package com.dansdev.selfcartmanager.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.dansdev.selfcartmanager.BuildConfig
import com.dansdev.selfcartmanager.data.db.converter.CartConverters
import com.dansdev.selfcartmanager.data.db.converter.ItemConverters
import com.dansdev.selfcartmanager.data.db.dao.CartDao
import com.dansdev.selfcartmanager.data.db.dao.ItemDao
import com.dansdev.selfcartmanager.data.db.dao.ProductDao
import com.dansdev.selfcartmanager.data.model.CartEntity
import com.dansdev.selfcartmanager.data.model.ItemEntity
import com.dansdev.selfcartmanager.data.model.ProductEntity

@Database(
    entities = [ItemEntity::class, ProductEntity::class, CartEntity::class],
    version = BuildConfig.DB_VERSION
)
@TypeConverters(
    CartConverters::class,
    ItemConverters::class
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun cartDao(): CartDao

    abstract fun itemsDao(): ItemDao

    abstract fun productDao(): ProductDao
}
