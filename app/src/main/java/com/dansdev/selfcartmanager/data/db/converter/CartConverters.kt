package com.dansdev.selfcartmanager.data.db.converter

import androidx.room.TypeConverter
import com.dansdev.selfcartmanager.domain.model.Cart
import java.util.*

class CartConverters {

    @TypeConverter
    fun mapStatusToString(status: Cart.Status?): String? = status?.name

    @TypeConverter
    fun mapStringToStatus(status: String?): Cart.Status? = status?.let { Cart.Status.valueOf(it) }

    @TypeConverter
    fun mapDateToLong(date: Date?): Long? = date?.time

    @TypeConverter
    fun mapLongToDate(date: Long?) = date?.let { Date(it) }
}
