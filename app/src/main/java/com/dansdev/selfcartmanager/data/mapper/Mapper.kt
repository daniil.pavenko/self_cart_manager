package com.dansdev.selfcartmanager.data.mapper

interface Mapper<SOURCE, OUTPUT> {

    fun map(source: SOURCE): OUTPUT

    fun list(source: List<SOURCE>): List<OUTPUT> {
        return source.map { map(it) }
    }
}
