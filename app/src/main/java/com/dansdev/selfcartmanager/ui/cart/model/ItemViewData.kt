package com.dansdev.selfcartmanager.ui.cart.model

import androidx.annotation.StringRes

data class ItemViewData(
    val photo: String?,
    val name: String,
    val price: String,
    val qty: Int,
    val packagingType: String,
    val subTotal: String,
    val subTotalPrice: Int,
    @StringRes val indicatorSubstitutableResId: Int,
    val indicatorSubstitutableColor: Int,
)
