package com.dansdev.selfcartmanager.ui.cart.adapter

enum class SortBy {
    DEFAULT, NAME, PRICE
}
