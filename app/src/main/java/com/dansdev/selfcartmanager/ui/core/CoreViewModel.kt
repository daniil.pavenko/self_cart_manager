package com.dansdev.selfcartmanager.ui.core

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.dansdev.selfcartmanager.ui.core.model.PopupMessage
import com.dansdev.selfcartmanager.ui.core.model.ProgressState

abstract class CoreViewModel: ViewModel() {

    protected val _progressState = MutableLiveData<ProgressState>()
    protected val _popupMessage = MutableLiveData<PopupMessage>()


    fun onProgressStateUpdate(): LiveData<ProgressState> = _progressState
    fun onPopupMessageUpdate(): LiveData<PopupMessage> = _popupMessage
}
