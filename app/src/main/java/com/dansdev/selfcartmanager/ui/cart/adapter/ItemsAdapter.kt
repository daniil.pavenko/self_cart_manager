package com.dansdev.selfcartmanager.ui.cart.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.dansdev.selfcartmanager.R
import com.dansdev.selfcartmanager.databinding.ItemGoodsBinding
import com.dansdev.selfcartmanager.ui.cart.model.ItemViewData
import timber.log.Timber

class ItemsAdapter : RecyclerView.Adapter<ItemsAdapter.ViewHolder>() {

    private val items = mutableListOf<ItemViewData>()

    var sortBy = SortBy.DEFAULT
        set(value) {
            if (value != field) {
                field = value
                update(items)
            } else {
                Timber.d("You are choose same sort type")
            }
        }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val binding = ItemGoodsBinding.bind(view)

        fun bind(item: ItemViewData) {
            binding.ivPhoto.load(item.photo) {
                error(R.drawable.ic_n_a_placeholder)
            }
            binding.tvName.text = item.name
            binding.tvPrice.text = itemView.resources.getString(R.string.label_price, item.price)
            binding.tvQty.text = itemView.resources.getString(R.string.label_qty, item.qty)
            binding.tvPackagingType.text = itemView.resources.getString(
                R.string.label_packaging_type,
                item.packagingType
            )
            binding.tvSubTotal.text =
                itemView.resources.getString(R.string.label_total, item.subTotal)
            binding.tvIndicatorSubstitutable.setBackgroundColor(item.indicatorSubstitutableColor)
            binding.tvIndicatorSubstitutable.setText(item.indicatorSubstitutableResId)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.item_goods,
            parent,
            false
        )
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int = items.size

    fun update(items: List<ItemViewData>) {
        val newList = when (sortBy) {
            SortBy.DEFAULT -> items
            SortBy.NAME -> items.sortedBy { it.name }
            SortBy.PRICE -> items.sortedByDescending { it.subTotalPrice }
        }
        val diffUtil = DiffUtil.calculateDiff(ItemsUtilCallback(newList, this.items))
        this.items.clear()
        this.items.addAll(newList)
        diffUtil.dispatchUpdatesTo(this)
    }

    private inner class ItemsUtilCallback(
        private val newItems: List<ItemViewData>,
        private val oldItems: List<ItemViewData>,
    ) : DiffUtil.Callback() {
        override fun getOldListSize(): Int = oldItems.size

        override fun getNewListSize(): Int = newItems.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldItems[oldItemPosition] == newItems[newItemPosition]
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldItems[oldItemPosition] == newItems[newItemPosition]
        }
    }
}
