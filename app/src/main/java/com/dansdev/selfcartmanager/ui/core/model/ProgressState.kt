package com.dansdev.selfcartmanager.ui.core.model

sealed class ProgressState {

    object Show : ProgressState()
    object Hide : ProgressState()

    data class Progress(val progress: Int, val max: Int) : ProgressState()

    fun isProgress(): Boolean = this == Show || this is Progress
}
