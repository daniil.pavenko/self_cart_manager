package com.dansdev.selfcartmanager.ui.util

import android.graphics.Color
import android.view.View
import androidx.annotation.StringRes
import com.google.android.material.snackbar.Snackbar

fun View.snackbarErr(message: String) {
    Snackbar.make(this, message, Snackbar.LENGTH_SHORT).apply {
        setBackgroundColor(Color.RED)
        setTextColor(Color.WHITE)
    }.show()
}

fun View.snackbarErr(@StringRes message: Int) {
    Snackbar.make(this, message, Snackbar.LENGTH_SHORT).apply {
        setBackgroundColor(Color.RED)
        setTextColor(Color.WHITE)
    }.show()
}

fun View.snackbar(@StringRes message: Int) {
    Snackbar.make(this, message, Snackbar.LENGTH_SHORT).show()
}
