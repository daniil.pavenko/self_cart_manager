package com.dansdev.selfcartmanager.ui.cart

import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DividerItemDecoration
import com.dansdev.selfcartmanager.R
import com.dansdev.selfcartmanager.ui.cart.adapter.ItemsAdapter
import com.dansdev.selfcartmanager.ui.cart.adapter.SortBy
import com.dansdev.selfcartmanager.ui.core.CoreFragment
import com.dansdev.selfcartmanager.ui.core.model.ProgressState
import com.dansdev.selfcartmanager.ui.util.formattedMoney
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class CartDetailFragment : CoreFragment<CartDetailViewModel>(R.layout.fragment_cart_detail) {

    override val viewModel: CartDetailViewModel by viewModel()

    private val adapter by lazy { ItemsAdapter() }

    override fun onSetupObservers(viewModel: CartDetailViewModel) {
        viewModel.onCartTotalUpdate().observe(viewLifecycleOwner) { cartTotalCents ->
            (activity as? AppCompatActivity)?.supportActionBar?.apply {
                subtitle = resources.getString(
                    R.string.label_cart_total,
                    cartTotalCents.formattedMoney()
                )
            }
        }
        viewModel.onItemsUpdate().observe(viewLifecycleOwner) { items ->
            adapter.update(items)
            binding.tvEmptyMessage.isVisible = items.isEmpty()
        }
    }

    override fun onProgressStateChanged(progressState: ProgressState) {
        binding.refreshLayout.isRefreshing = progressState.isProgress()
    }

    override fun onViewReady() {
        setupAdapter()
        setupListeners()
        setupMenu()

        viewModel.load()
    }

    private fun setupMenu() {
        setHasOptionsMenu(true)
    }

    private fun setupAdapter() {
        binding.rvGoods.adapter = adapter

        if (binding.rvGoods.itemDecorationCount == 0) {
            val divider = DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL)
            ContextCompat.getDrawable(requireContext(), R.drawable.divider_goods_list)?.let {
                divider.setDrawable(it)
            }
            binding.rvGoods.addItemDecoration(divider)
        }
    }

    private fun setupListeners() {
        binding.refreshLayout.setOnRefreshListener { viewModel.load() }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_cart_detail, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.itemSortByName -> adapter.sortBy = SortBy.NAME
            R.id.itemSortByTotalPrice -> adapter.sortBy = SortBy.PRICE
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {

        const val TAG = "CartDetailFragment"

        fun newInstance() = CartDetailFragment()
    }
}
