package com.dansdev.selfcartmanager.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commit
import com.dansdev.selfcartmanager.databinding.ActivityMainBinding
import com.dansdev.selfcartmanager.ui.cart.CartDetailFragment
import kotlin.properties.Delegates

class MainActivity : AppCompatActivity() {

    private var binding: ActivityMainBinding by Delegates.notNull()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)
        supportFragmentManager.commit {
            replace(
                binding.fragmentContainer.id,
                CartDetailFragment.newInstance(),
                CartDetailFragment.TAG
            )
        }
    }
}
