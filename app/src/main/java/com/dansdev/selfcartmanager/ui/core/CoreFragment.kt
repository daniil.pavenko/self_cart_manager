package com.dansdev.selfcartmanager.ui.core

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import com.dansdev.selfcartmanager.databinding.FragmentCartDetailBinding
import com.dansdev.selfcartmanager.ui.core.model.PopupMessage
import com.dansdev.selfcartmanager.ui.core.model.ProgressState
import com.dansdev.selfcartmanager.ui.util.snackbar
import com.dansdev.selfcartmanager.ui.util.snackbarErr
import com.google.android.material.snackbar.Snackbar
import kotlin.properties.Delegates

abstract class CoreFragment<VM : CoreViewModel>(@LayoutRes fragmentLayoutId: Int) :
    Fragment(fragmentLayoutId) {

    protected var binding: FragmentCartDetailBinding by Delegates.notNull()

    protected abstract val viewModel: VM

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)
            ?: throw NullPointerException("Please setup layout id into constructor")
        binding = FragmentCartDetailBinding.bind(view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupObservers()
        onSetupObservers(viewModel)
        onViewReady()
    }

    private fun setupObservers() {
        viewModel.onProgressStateUpdate().observe(viewLifecycleOwner) { progressState ->
            onProgressStateChanged(progressState)
        }
        viewModel.onPopupMessageUpdate().observe(viewLifecycleOwner) { popupMessage ->
            when (popupMessage) {
                is PopupMessage.CustomFail -> binding.root.snackbarErr(popupMessage.message)
                is PopupMessage.LocalFail -> binding.root.snackbarErr(popupMessage.message)
                is PopupMessage.Success -> binding.root.snackbar(popupMessage.message)
            }
        }
    }

    abstract fun onProgressStateChanged(progressState: ProgressState)
    abstract fun onSetupObservers(viewModel: VM)
    abstract fun onViewReady()
}
