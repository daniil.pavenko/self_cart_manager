package com.dansdev.selfcartmanager.ui.cart

import android.graphics.Color
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.dansdev.selfcartmanager.R
import com.dansdev.selfcartmanager.data.repository.ICartRepository
import com.dansdev.selfcartmanager.data.repository.exception.EmptyCartException
import com.dansdev.selfcartmanager.domain.model.Item
import com.dansdev.selfcartmanager.ui.cart.model.ItemViewData
import com.dansdev.selfcartmanager.ui.core.CoreViewModel
import com.dansdev.selfcartmanager.ui.core.model.PopupMessage
import com.dansdev.selfcartmanager.ui.core.model.ProgressState
import com.dansdev.selfcartmanager.ui.util.formattedMoney
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CartDetailViewModel(private val repository: ICartRepository) : CoreViewModel() {

    private val _cartTotalLiveData = MutableLiveData<Int>()
    private val _itemsLiveData = MutableLiveData<List<ItemViewData>>()

    fun onCartTotalUpdate(): LiveData<Int> = _cartTotalLiveData
    fun onItemsUpdate(): LiveData<List<ItemViewData>> = _itemsLiveData

    fun load() {
        viewModelScope.launch(Dispatchers.IO) {
            _progressState.postValue(ProgressState.Show)
            val itemsResult = repository.getCart()
            if (itemsResult.isSuccess) {
                itemsResult.getOrNull()?.let { cart ->
                    _cartTotalLiveData.postValue(cart.cartTotal)
                    _itemsLiveData.postValue(prepareItemsData(cart.orderItemsInformation))
                } ?: run {
                    _popupMessage.postValue(PopupMessage.LocalFail(R.string.error_cart_not_exist))
                }
            } else {
                when (itemsResult.exceptionOrNull()) {
                    is EmptyCartException -> _itemsLiveData.postValue(emptyList())
                    else -> _popupMessage.postValue(PopupMessage.LocalFail(R.string.error_cant_load_cart))
                }
            }
            _progressState.postValue(ProgressState.Hide)
        }
    }

    private fun prepareItemsData(orderItemsInformation: List<Item>): List<ItemViewData> {
        return orderItemsInformation.map {
            val subTotalPrice = (it.quantity * it.product.wholePrice())
            ItemViewData(
                it.photoByPackageType(),
                it.product.name,
                it.product.wholePrice().formattedMoney(),
                it.quantity,
                it.packagingType.name,
                subTotalPrice.formattedMoney(),
                subTotalPrice,
                if (it.substitutable) R.string.label_substitutable_available
                else R.string.label_substitutable_unavailable,
                if (it.substitutable) Color.GREEN else Color.LTGRAY
            )
        }
    }
}
