package com.dansdev.selfcartmanager.ui.core.model

import androidx.annotation.StringRes

sealed class PopupMessage {
    data class Success(@StringRes val message: Int) : PopupMessage()
    data class LocalFail(@StringRes val message: Int) : PopupMessage()
    data class CustomFail(val message: String) : PopupMessage()
}
