package com.dansdev.selfcartmanager.ui.util

fun Int.formattedMoney(currencySymbol: String = "$"): String {
    return String.format("%.2f %s", this.toDouble() / 100, currencySymbol)
}
