package com.dansdev.selfcartmanager.di

import org.koin.core.qualifier.Qualifier
import org.koin.core.qualifier.named

object DIQualifier {

    val REMOTE_CART_DATASOURCE = named("remote_cart_data_source")
    val LOCAL_CART_DATASOURCE = named("local_cart_data_source")
}
