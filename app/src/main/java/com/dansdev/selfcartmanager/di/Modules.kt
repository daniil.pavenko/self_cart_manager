package com.dansdev.selfcartmanager.di

import android.net.ConnectivityManager
import androidx.core.content.ContextCompat
import androidx.room.Room
import com.dansdev.selfcartmanager.BuildConfig
import com.dansdev.selfcartmanager.data.db.AppDatabase
import com.dansdev.selfcartmanager.data.repository.ICartDataSource
import com.dansdev.selfcartmanager.data.repository.ICartRepository
import com.dansdev.selfcartmanager.data.repository.impl.CartRepositoryImpl
import com.dansdev.selfcartmanager.data.repository.impl.LocalCartDataSource
import com.dansdev.selfcartmanager.data.repository.impl.RemoteCartDataSource
import com.dansdev.selfcartmanager.domain.api.ResultAdapterFactory
import com.dansdev.selfcartmanager.domain.api.SelfCartService
import com.dansdev.selfcartmanager.domain.api.util.CustomDateAdapter
import com.dansdev.selfcartmanager.domain.api.util.NetworkManager
import com.dansdev.selfcartmanager.domain.api.util.NetworkManagerImpl
import com.dansdev.selfcartmanager.ui.cart.CartDetailViewModel
import com.squareup.moshi.Moshi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.create
import java.util.*

val appModule = module {
    single {
        ContextCompat.getSystemService(
            get(),
            ConnectivityManager::class.java
        )
    }
    single<NetworkManager> { NetworkManagerImpl(get()) }
    single {
        OkHttpClient.Builder()
            .addInterceptor(
                HttpLoggingInterceptor().apply {
                    if (BuildConfig.DEBUG) setLevel(HttpLoggingInterceptor.Level.BODY)
                    else setLevel(HttpLoggingInterceptor.Level.BASIC)
                }
            )
            .build()
    }
    single<Moshi> {
        Moshi.Builder()
            .add(Date::class.java, CustomDateAdapter().nullSafe())
            .build()
    }
    single<Retrofit> {
        Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL + "${BuildConfig.API_VERSION}/")
            .addCallAdapterFactory(ResultAdapterFactory())
            .client(get())
            .addConverterFactory(MoshiConverterFactory.create(get()))
            .build()
    }

    single<SelfCartService> { get<Retrofit>().create() }
}

val dataBaseModule = module {
    single {
        Room.databaseBuilder(
            get(),
            AppDatabase::class.java,
            BuildConfig.DB_NAME
        ).build()
    }
    single<ICartDataSource>(qualifier = DIQualifier.REMOTE_CART_DATASOURCE) {
        RemoteCartDataSource(get())
    }
    single<ICartDataSource>(qualifier = DIQualifier.LOCAL_CART_DATASOURCE) {
        LocalCartDataSource(get())
    }
    single<ICartRepository> {
        CartRepositoryImpl(
            get(qualifier = DIQualifier.LOCAL_CART_DATASOURCE),
            get(qualifier = DIQualifier.REMOTE_CART_DATASOURCE),
            get()
        )
    }
}

val appViewModelModule = module {
    viewModel { CartDetailViewModel(get()) }
}