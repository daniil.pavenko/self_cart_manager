package com.dansdev.selfcartmanager.domain.api

import com.dansdev.selfcartmanager.domain.model.Cart
import retrofit2.http.GET

interface SelfCartService {

    @GET("59c791ed1100005300c39b93")
    suspend fun loadCart(): Result<Cart>
}
