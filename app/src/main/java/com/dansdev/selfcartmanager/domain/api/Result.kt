package com.dansdev.selfcartmanager.domain.api

sealed class Result<out T> {

    sealed class Success<T> : Result<T>() {

        abstract val value: T

        override fun toString() = "Success($value)"

        class Value<T>(override val value: T) : Success<T>()

        data class HttpResponse<T>(
            override val value: T,
            override val statusCode: Int,
            override val statusMessage: String? = null,
            override val url: String? = null
        ) : Success<T>(), com.dansdev.selfcartmanager.domain.api.HttpResponse

        object Empty : Success<Nothing>() {

            override val value: Nothing get() = error("No value")

            override fun toString() = "Success"
        }
    }


    sealed class Failure<E : Throwable>(open val error: E? = null) : Result<Nothing>() {

        override fun toString() = "Failure($error)"

        class Error(override val error: Throwable) : Failure<Throwable>(error)

        class HttpError(override val error: HttpException) : Failure<HttpException>(),
            HttpResponse {

            override val statusCode: Int get() = error.statusCode

            override val statusMessage: String? get() = error.statusMessage

            override val url: String? get() = error.url
        }
    }
}

typealias EmptyResult = Result<Nothing>

fun <T> Result<T>.isSuccess(): Boolean {
    return this is Result.Success
}

fun <T> Result<T>.asSuccessOrNull(): Result.Success<T>? {
    return if (this.isSuccess()) {
        this as Result.Success<T>
    } else null
}

fun <T> Result<T>.asFailure(): Result.Failure<*> {
    return this as Result.Failure<*>
}

fun <T> Result<T>.asKotlinResult(): kotlin.Result<T> {
    return if (this.isSuccess()) {
        this.asSuccessOrNull()?.value?.let { data ->
            kotlin.Result.success(data)
        } ?: kotlin.Result.failure(Throwable("Null data"))
    } else {
        val failure = this.asFailure()
        if (failure is Result.Failure.HttpError) {
            kotlin.Result.failure(failure.error.fillInStackTrace())
        } else {
            kotlin.Result.failure(failure.error ?: Throwable("API error"))
        }
    }
}
