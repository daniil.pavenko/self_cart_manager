package com.dansdev.selfcartmanager.domain.api.util

import android.net.ConnectivityManager
import android.net.NetworkCapabilities

interface NetworkManager {

    fun hasConnection(): Boolean
}

class NetworkManagerImpl(
    private val connectivityManager: ConnectivityManager?
) : NetworkManager {

    override fun hasConnection(): Boolean {
        val networkCapabilities = connectivityManager?.activeNetwork ?: return false
        val actNw = connectivityManager.getNetworkCapabilities(networkCapabilities) ?: return false
        return when {
            actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
            actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
            actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
            else -> false
        }
    }
}
