package com.dansdev.selfcartmanager.domain.model

import com.squareup.moshi.Json

data class Product(
    @field:Json(name = "id") val id: Long,
    @field:Json(name = "name") val name: String,
    @field:Json(name = "sku") val sku: Int,
    @field:Json(name = "unit_photo_filename") val unitPhotoFilename: String?,
    @field:Json(name = "weight_photo_filename") val weightPhotoFilename: String?,
    @field:Json(name = "pack_photo_file") val packPhotoFile: String?,
    @field:Json(name = "unit_price") val unitPrice: Int,
    @field:Json(name = "case_price") val casePrice: Int,
    @field:Json(name = "weight_price") val weightPrice: Int,
) {
    fun wholePrice(): Int = weightPrice + casePrice + unitPrice
}
