package com.dansdev.selfcartmanager.domain.model

import com.squareup.moshi.Json

data class Item(
    @field:Json(name = "id") val id: Long,
    @field:Json(name = "quantity") val quantity: Int,
    @field:Json(name = "product_id") val productId: Long,
    @field:Json(name = "sub_total") val subTotal: Int,
    @field:Json(name = "order_id") val orderId: Int,
    @field:Json(name = "packaging_type") val packagingType: PackagingType,
    @field:Json(name = "substitutable") val substitutable: Boolean,
    @field:Json(name = "product") val product: Product
) {
    fun photoByPackageType(): String? = when (packagingType) {
        PackagingType.UNIT -> product.unitPhotoFilename
        PackagingType.WEIGHT -> product.weightPhotoFilename
        PackagingType.CASE -> product.packPhotoFile
    }

    enum class PackagingType {
        @field:Json(name = "unit") UNIT,
        @field:Json(name = "weight") WEIGHT,
        @field:Json(name = "case") CASE
    }
}
