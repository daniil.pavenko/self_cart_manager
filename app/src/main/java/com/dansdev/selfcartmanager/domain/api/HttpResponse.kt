package com.dansdev.selfcartmanager.domain.api

interface HttpResponse {

    val statusCode: Int

    val statusMessage: String?

    val url: String?
}
