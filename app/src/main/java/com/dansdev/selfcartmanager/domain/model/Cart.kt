package com.dansdev.selfcartmanager.domain.model

import com.squareup.moshi.Json
import java.util.*

data class Cart(
    @field:Json(name = "id") val id: Long = 0,
    @field:Json(name = "cart_total") val cartTotal: Int = 0,
    @field:Json(name = "total") val total: Int = 0,
    @field:Json(name = "delivery_fee") val deliveryFee: Int = 0,
    @field:Json(name = "created_at_iso8601") val createdAt: Date? = null,
    @field:Json(name = "last_time_modified_int") val lastTimeModifiedTimestamp: Long = 0,
    @field:Json(name = "delivery_date_iso8601") val deliveryDate: Date? = null,
    @field:Json(name = "status") val status: Status? = null,
    @field:Json(name = "order_items_information") val orderItemsInformation: List<Item> = emptyList(),
    @field:Json(name = "restaurant_id") val restaurantId: Long = 0,
    @field:Json(name = "note") val note: String? = null,
    @field:Json(name = "same_day_charge_amount") val sameDayChargeAmount: Int = 0,
    @field:Json(name = "sub_total") val subTotal: Int = 0,
    @field:Json(name = "error_description") val errorDescription: String? = null
) {
    enum class Status {
        @field:Json(name = "new") NEW,
        @field:Json(name = "in_progress") IN_PROGRESS,
        @field:Json(name = "completed") COMPLETED,
        @field:Json(name = "archived") ARCHIVED,
    }
}
