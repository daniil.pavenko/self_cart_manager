package com.dansdev.selfcartmanager

import android.app.Application
import com.dansdev.selfcartmanager.di.appModule
import com.dansdev.selfcartmanager.di.appViewModelModule
import com.dansdev.selfcartmanager.di.dataBaseModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import timber.log.Timber

class SCartApp : Application() {

    override fun onCreate() {
        super.onCreate()
        setupTimber()
        setupKoin()
    }

    private fun setupTimber() {
        if (BuildConfig.DEBUG) Timber.plant(Timber.DebugTree())
    }

    private fun setupKoin() {
        startKoin {
            androidLogger(Level.ERROR)
            androidContext(this@SCartApp)
            modules(appModule, dataBaseModule, appViewModelModule)
        }
    }
}
